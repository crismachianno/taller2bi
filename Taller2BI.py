from os import environ
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import pandas as pd
import time
import sys
from tabulate import tabulate
from sqlalchemy import create_engine

# Autenticacion en spotify, retorna un usuario autenticado.
def authenticate(cliend_id: str, client_secret: str) -> spotipy.client.Spotify:
    sp = spotipy.Spotify(
        client_credentials_manager=SpotifyClientCredentials(
            client_id=cliend_id,
            client_secret=client_secret
        )
    )

    return sp

#Metodo para obtener un artista, usando como critero de busqueda el nombre del artista.
def getArtista (sp: spotipy.client.Spotify, artista: str):
    if len(sys.argv) > 1:
        name = ' '.join(sys.argv[1:])
    else:
        name = artista
    results = sp.search(q='artist:' + name, type='artist')
    items = results['artists']['items']
    if len(items) > 0:
        artist = items[0]
        return artist

#Metodo para llenar el dataframe de artistas, con el metodo getArtista de spotipy, seleccionamos la informacion que queremos
# incluir en el dataframe y lo agreamos
def llenarArtistas(dataframe:pd.DataFrame,artistas:[str],sp: spotipy.client.Spotify):
    index=0
    for artista in artistas:
        art=getArtista(sp,artista)
        dataframe.loc[index]=[art['name'], art['popularity'], art['type'],art['external_urls']['spotify'], art['followers']['total'], art['href'], time.time() , "Api Spotify" ]
        index+=1
    return

#Metodo para llenar el dataframe de canciones, iteramos los artistas y por cada uno obtenemos el top10 de las canciones
#luego de ellas obtenemos la informacion que se incluirá en el dataframe y la agregamos fila por fila.
def llenarCanciones(dataframeCanciones:pd.DataFrame,dataframeArtistas:pd.DataFrame,sp: spotipy.client.Spotify):
    index: int=0
    for i in dataframeArtistas.index:
        results=sp.artist_top_tracks(dataframeArtistas['Url'][i])
        for iterador in range(10):
            dataframeCanciones.loc[index] = [results['tracks'][iterador]['name'], results['tracks'][iterador]['type'], results['tracks'][iterador]['artists'][0]['name'],
                                     results['tracks'][iterador]['album']['name'], results['tracks'][iterador]['track_number'],
                                     results['tracks'][iterador]['popularity'], results['tracks'][iterador]['id'],
                                     results['tracks'][iterador]['external_urls']['spotify'], results['tracks'][iterador]['album']['release_date'],
                                     getArtista(sp,(results['tracks'][iterador]['artists'][0]['name']))["genres"], time.time(), "Api Spotify"]
            index=index+1
    return


if __name__ == "__main__":
    # Obtenemos los datos de acceso de las variables del Sistema
    CLIENT_ID = environ.get("SPOTIFY_CLIENT_ID")
    CLIENT_SECRET = environ.get("SPOTIFY_CLIENT_SECRET")
    # Creamos una conexion con la API Spotify y obtenemos un usuario autenticado para la interaccion
    sp_instance = authenticate(CLIENT_ID, CLIENT_SECRET)
    #Definimos las columnas que usaremos en el DataFrame de Artistas y lo creamos.
    artist_column_names = ["Nombre", "Popularaidad", "Tipo", "Url", "Seguidores", "Href", "Fecha_Carga", "Origen"]
    artistasDF = pd.DataFrame(columns=artist_column_names)
    #Definomos en una lista los artistas sobre los cuales obtendremos la informacion de la API.
    artistas=["Radiohead","Metallica","Black Sabbath","Led Zeppelin","Bob Dylan","Nirvana","The Doors","Heroes Del Silencio","Soda Stereo","Luis Alberto Spinetta"]
    #Llenamos el dataframe con la informacion de los artistas, usamos el metodo llenarArtistas
    llenarArtistas(artistasDF,artistas,sp_instance)
    # Definimos las columnas que usaremos en el DataFrame de Canciones y lo creamos.
    canciones_column_names=["Nombre","Tipo","Artista","Album","# Cancion","Popularidad","Id","Url","Fecha Lanzamiento","Genero","Fecha_Carga","Origen"]
    cancionesDF=pd.DataFrame(columns=canciones_column_names)
    #llenamos el dataframe de canciones con el top 10 de las canciones del dataframe artistas.
    llenarCanciones(cancionesDF,artistasDF,sp_instance)
    #imprimimos la totalidad de los dataset de artistas y canciones, con la ayuda de la libreria tabulate
    print(tabulate(artistasDF, headers='keys', tablefmt='psql'))
    print(tabulate(cancionesDF, headers='keys', tablefmt='psql'))
    #Establecemos la conexion con la base de datos, en este ejemplo usamos postgres, remplace ###### con la contraseña de postgres
    engine = create_engine('postgresql://postgres:######@127.0.0.1:5432/postgres')
    #procedemos con el cargue de los dataset a nuestra BD, usamos tablas separadas para cada uno.
    artistasDF.to_sql('Artistas', con=engine, index=False, if_exists='replace')
    cancionesDF.to_sql('Canciones', con=engine, index=False, if_exists='replace')
